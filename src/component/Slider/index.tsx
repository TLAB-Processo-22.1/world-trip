import { Flex, Heading, Image, Link } from "@chakra-ui/react";
import { Slide } from "react-slideshow-image";
import 'react-slideshow-image/dist/styles.css';

export function Slider() {

  return (
    <Slide>

      <Flex
        alignItems='center'
        justifyContent='center'
        backgroundSize='cover'
        h='350px'

      >
        <Flex>
          <Image src='./europa.jpg' />
        </Flex>
      </Flex>

      <Flex
        alignItems='center'
        justifyContent='center'
        backgroundSize='cover'
        h='350px'

      >
        <Flex>
          <Image src='./slide.jpg' />
        </Flex>
      </Flex>

      <Flex
        alignItems='center'
        justifyContent='center'
        backgroundSize='cover'
        h='350px'

      >
        <Flex>
          <Image src='./slide2.jpg' />
        </Flex>
      </Flex>
   </Slide>

  );
}
