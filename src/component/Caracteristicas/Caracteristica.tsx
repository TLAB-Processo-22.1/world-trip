import { Flex, useBreakpointValue, Image, Text } from "@chakra-ui/react";
import { StarIcon } from '@chakra-ui/icons'


interface CaracteristicaProps {
  icon: string;
  text: string
}

export function Caracteristica({icon, text}: CaracteristicaProps){
  //hook do chakra para mostrar se estar no celular ou nao
  const isMobile = useBreakpointValue({
    base: false,
    sm: true
  })
  
  return(
    <>
    {/*Flex que varia em linha e coluna*/}
      <Flex
        direction={['row', 'column']}
        align='center'
        justify='center'
     >

       {isMobile ? <Image src={`/icons/${icon}.svg`} w='85px' h='85px' mb='6' /> : <StarIcon/>} 
          <Text mr="30px" fontWeight='600' align='center' color='gray.700' fontSize={['md', 'xl', '2xl']}>{text}</Text>
        
      </Flex>
    </>
  );
}