import {Flex, Heading} from '@chakra-ui/react'
import {Header} from '../src/component/Header'
import { Banner } from './component/Banner';
import { Caracteristicas } from './component/Caracteristicas';
import { Slider } from './component/Slider';
import { Separator } from './Separator';
export function App() {
  return (
    <Flex direction='column'>
      <Header/>
      <Banner/>
      <Caracteristicas/>
      <Separator/>

      <Heading
        textAlign='center'
        fontWeight='500'
        mb={['5', '14']}
        fontSize={['lg', '3xl', '4xl']}
      >
        Vamos nessa? <br/> Então escolha seu continente
      </Heading>
        
      <Slider />
    </Flex>
  );
}
